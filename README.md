## How to Use

1. Add in services.yaml 
---
    request_body_converter:
        class: ParamConverter\RequestBodyConverter
        tags:
            - { name: request.param_converter, converter: converter.request_body }
---
2. Add in controller action 
---
    /**
     * @ParamConverter("dto", converter="converter.request_body")
     */
    public function editAction(Dto $dto): JsonResponse
    {
---
