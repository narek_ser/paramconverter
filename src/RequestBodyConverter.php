<?php

namespace ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class RequestBodyConverter implements ParamConverterInterface
{
    private SerializerInterface $serializer;
    private array $context = [];

    /**
     * @param SerializerInterface $serializer
     * @param string[]|null $groups An array of groups to be used in the serialization context
     * @param string|null $version A version string to be used in the serialization context
     */
    public function __construct(
        SerializerInterface $serializer,
        ?array $groups = null,
        ?string $version = null
    ) {
        $this->serializer = $serializer;

        if (!empty($groups)) {
            $this->context['groups'] = (array)$groups;
        }

        if (!empty($version)) {
            $this->context['version'] = $version;
        }

    }

    /**
     * @inheritDoc
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $options = (array)$configuration->getOptions();
        
        try {
            $object = $this->serializer->deserialize(
                $request->getContent(),
                $configuration->getClass(),
                $request->getContentType()
            );
        } catch (UnsupportedFormatException $e) {
            return $this->throwException(new UnsupportedMediaTypeHttpException($e->getMessage(), $e), $configuration);
        } catch (SymfonySerializerException $e) {
            return $this->throwException(new BadRequestHttpException($e->getMessage(), $e), $configuration);
        }

        $request->attributes->set($configuration->getName(), $object);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function supports(ParamConverter $configuration): bool
    {
        return null !== $configuration->getClass() && 'converter.request_body' === $configuration->getConverter();
    }

    /**
     * @param \Exception $exception
     * @param ParamConverter $configuration
     * @return bool
     * @throws \Exception
     */
    private function throwException(\Exception $exception, ParamConverter $configuration)
    {
        if ($configuration->isOptional()) {
            return false;
        }

        throw $exception;
    }
}